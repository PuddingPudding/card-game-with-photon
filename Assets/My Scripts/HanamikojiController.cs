﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//花見小路的控制器，目標為接收輸入(選擇什麼行動，接著選擇本行動將打出手中哪幾張牌)，確認完後再去呼叫資料層
public class HanamikojiController
{
    /* 需要能做到
     * 接收出牌指令
     * 接收選牌事件 (對手使用 "贈予" 或 "競爭")
     */

    public Action<Card> PlayCardEvent;
    
    /// <summary>
    /// 是否為當前回合的玩家
    /// </summary>
    public bool IsCurrentPlayer { get; protected set; }
    /// <summary>
    /// 選擇區的手牌編號 (當前準備甩掉哪些牌)
    /// </summary>
    public List<int> CardsChosen { get; } = new List<int>();
    /// <summary>
    /// 對手送到該玩家面前的卡牌 (例如贈予/競爭時出現)
    /// </summary>
    public List<HanamikojiGift> CardsFromOpponent { get { return m_player.CardsFromOpponent; } }
    /// <summary>
    /// 當前準備執行哪些動作
    /// </summary>
    public EHanamikojiAction m_currentAction = EHanamikojiAction.SECRET_MEETING;
    private HanamikojiPlayer m_player; //由你操控的玩家實體

    public HanamikojiController()
    {
        m_player = new HanamikojiPlayer();
        m_player.AddCard(new Card("a"));
        m_player.AddCard(new Card("a"));
        m_player.AddCard(new Card("b"));
        m_player.AddCard(new Card("b"));
        m_player.AddCard(new Card("c"));
        m_player.AddCard(new Card("c"));

        m_currentAction = EHanamikojiAction.SECRET_MEETING;
        CardsChosen = new List<int>();
    }
    public HanamikojiController(HanamikojiPlayer _player)
    {
        m_player = _player;
        CardsChosen = new List<int>();
        this.IsCurrentPlayer = false;
    }

    /// <summary>
    /// 帶入行動並獲得該行動可以出牌的數量 (若有錯誤的話回傳-1)
    /// </summary>
    /// <param name="_action">花見小路的行動</param>
    /// <returns></returns>
    public int InputAction(EHanamikojiAction _action)
    {
        CardsChosen.Clear();
        if(this.m_player.CheckActionLeft(_action) )
        {
            m_currentAction = _action;
            return HanamikojiPlayer.GetActionCardsNum(_action);
        }
        else
        {
            return -1;
        }
    }
    /// <summary>
    /// 主要是在選定行動後，決定要將哪張卡牌放到 "出牌區"
    /// </summary>
    /// <param name="_iIndex"></param>
    /// <returns>是否還未選過，或著已經不能再選</returns>
    public bool InputCardIndex(int _iIndex)
    {
        bool bIndexOK = (CardsChosen.Count < HanamikojiPlayer.GetActionCardsNum(m_currentAction));
        //確認現在選的卡牌數量是否還在合理範圍內
        for (int i = 0; i < CardsChosen.Count && bIndexOK; i++)
        {
            if (_iIndex == CardsChosen[i]) { bIndexOK = false; }
        }        
        if (bIndexOK) { CardsChosen.Add(_iIndex); }
        return bIndexOK;
    }
    /// <summary>
    /// 將第幾號(_iIndex)手牌從選擇區當中移除 (注意:並非依照選擇區的索引值判定)
    /// </summary>
    /// <param name="_iIndex"></param>
    /// <returns>是否有得移除</returns>
    public bool RemoveCardIndex(int _iIndex)
    {
        bool bIndexOK = false;
        for (int i = 0; i < CardsChosen.Count && !bIndexOK; i++)
        {
            if (_iIndex == CardsChosen[i])
            {
                bIndexOK = true;
                CardsChosen.RemoveAt(i);
            }
        }
        return bIndexOK;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_iIndex"></param>
    /// <returns></returns>
    public bool ResponseOpponent(int _iIndex)
    {
        bool bSuccess = (this.CardsFromOpponent != null);
        Debug.Log("回應時所選的牌: " + _iIndex + " 對手給的牌數有" + this.CardsFromOpponent.Count);
        if (bSuccess)
        {
            if(_iIndex >= 0  && _iIndex < this.CardsFromOpponent.Count)
            {
                this.m_player.ResponseOpponentAction(m_player.OpponentAction, _iIndex);
            }
            else { bSuccess = false; }
        }
        return bSuccess;
    }
    /// <summary>
    /// 執行動作
    /// </summary>
    public bool InvokeHanamikojiAction()
    {
        if(CardsChosen.Count == HanamikojiPlayer.GetActionCardsNum(m_currentAction) )
        {
            m_player.PlayAction(m_currentAction, CardsChosen);
            this.CardsChosen.Clear();
            return true;
        }
        else { return false; }
    }
    public string GetPlayerStatus()
    {
        return m_player.GetStatus();
    }
    public void SetCurrentPlayer(bool _bisCurrentPlayer)
    {
        this.IsCurrentPlayer = _bisCurrentPlayer;
        //如果電腦玩家Controller的話，被設為true的同時就會接著下來執行PlayAction判斷
    }
}
