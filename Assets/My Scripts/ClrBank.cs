﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Simple Color Bank")]
public class ClrBank : ScriptableObject
{
    [SerializeField]
    private Color[] m_arrClr;
    
    public Color[] Colors { get { return this.m_arrClr; } }
}
