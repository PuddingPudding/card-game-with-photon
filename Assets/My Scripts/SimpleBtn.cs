﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SimpleBtn : MonoBehaviour
{
    public Button btn;
    public TextMeshProUGUI txt;
    public Image cardFace;
    public TextMeshProUGUI[] arrTxt;
    public ClrBank colorBank;
    public SpriteBank spritesBank;

    private int[] m_arrCardNum = { 2, 2, 2, 3, 3, 4, 5 };

    /// <summary>
    /// 依照藝妓類別設定外觀
    /// </summary>
    /// <param name="_gType"></param>
    public void SetHanamikojiGiftFace(EGeishaType _gType)
    {
        this.cardFace.sprite = spritesBank.Sprites[(int)_gType];
        this.cardFace.color = colorBank.Colors[(int)_gType];
        for (int i = 0; i< this.arrTxt.Length; i++)
        {
            this.arrTxt[i].color = colorBank.Colors[(int)_gType];
            this.arrTxt[i].text = "" + this.m_arrCardNum[(int)_gType];
        }
    }
}
