﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//一個初次洗牌的測試
public class StarterTest : MonoBehaviour
{
    Deck m_deck;
    HanamikojiPlayer m_player;
    HanamikojiController m_controller;
    // Start is called before the first frame update
    void Start()
    {
        m_deck = new Deck();
        m_deck.AddCard(new Card("零"));
        m_deck.AddCard(new Card("一"));
        m_deck.AddCard(new Card("二"));
        m_deck.AddCard(new Card("三"));
        m_deck.AddCard(new Card("四"));
        m_deck.AddCard(new Card("五"));
        m_deck.AddCard(new Card("六"));
        m_deck.AddCard(new Card("七"));
        m_deck.AddCard(new Card("八"));
        m_deck.AddCard(new Card("九"));
        Debug.Log(m_deck.GetAllCardName());
        m_deck.Shuffle();
        Debug.Log(m_deck.GetAllCardName());

        //m_player = new HanamikojiPlayer();
        //m_player.AddCard(new Card("a"));
        //m_player.AddCard(new Card("a"));
        //m_player.AddCard(new Card("b"));
        //m_player.AddCard(new Card("b"));
        //m_player.AddCard(new Card("c"));
        //m_player.AddCard(new Card("c"));
        //Debug.Log(m_player.GetStatus());
        //m_player.PlayAction(EHanamikojiAction.TRADE_OFF, new List<int> { 4, 1 });
        //Debug.Log("使用取捨，放棄第4和第1張牌");
        //Debug.Log(m_player.GetStatus());

        m_controller = new HanamikojiController();
        Debug.Log(m_controller.GetPlayerStatus() );
        m_controller.InputAction(EHanamikojiAction.TRADE_OFF); //挑選行動
        m_controller.InputCardIndex(0); //挑選手牌編號
        m_controller.InputCardIndex(2);
        m_controller.InvokeHanamikojiAction();
        Debug.Log(m_controller.GetPlayerStatus());
        m_controller.InputAction(EHanamikojiAction.SECRET_MEETING); //挑選行動
        m_controller.InputCardIndex(1);
        m_controller.InvokeHanamikojiAction();
        Debug.Log(m_controller.GetPlayerStatus());
    }
}
