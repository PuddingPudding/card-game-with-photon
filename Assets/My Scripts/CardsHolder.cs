﻿using System.Collections.Generic;

//CardsHolder(卡牌持有者，它可能是指手牌/牌堆/棄牌區，主要是資料乘載者)
public abstract class CardsHolder
{
    public List<ICard> Cards { get; protected set; } 

    public CardsHolder()
    {
        Cards = new List<ICard>();
    }

    /// <summary>
    /// 加入卡牌 (直接加入，或著加入一張複製品)
    /// </summary>
    /// <param name="_newCard"></param>
    /// <param name="_bCopy">是否為複製的</param>
    public virtual void AddCard(ICard _newCard , bool _bCopy = false)
    {
        if (_bCopy) { this.Cards.Add(_newCard.Clone() ); }
        else { this.Cards.Add(_newCard); }
    }
    /// <summary>
    /// 將自己的整副牌清空並換成輸入的那些牌 (預設為直接指向來源牌堆)
    /// </summary>
    /// <param name="_bCopy"></param>
    public virtual void CopyCards(List<ICard> _cardsInput , bool _bCopy = false)
    {
        this.Cards.Clear();
        if(_bCopy)
        {
            for(int i = 0; i < _cardsInput.Count; i++)
            {
                this.Cards.Add(_cardsInput[i].Clone());
            }
        }
        else { this.Cards = _cardsInput; }
    }
}
