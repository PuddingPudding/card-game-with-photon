﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : CardsHolder
{
    /// <summary>
    /// 洗牌function，可以帶入洗的範圍，End值若小於等於Start值，視為整副洗
    /// </summary>
    /// <param name="_iStart">起點(包含)</param>
    /// <param name="_iEnd">終點(不包含)</param>
    /// <returns></returns>
    public virtual void Shuffle(int _iStart = 0 , int _iEnd = -1)
    {
        if (_iStart < 0) { _iStart = 0; }
        if (_iEnd <= _iStart) { _iEnd = base.Cards.Count; } //不能這麼做，當你的套牌沒有卡牌的時候會洗不了(無限迴圈)
        
        //進行洗牌動作
        for(int i = _iStart; i < _iEnd; i++)
        {
            int iTargetIndex = Random.Range(_iStart, _iEnd);
            ICard cardTemp = base.Cards[i];
            base.Cards[i] = base.Cards[iTargetIndex];
            base.Cards[iTargetIndex] = cardTemp;
        }
    }
    public string GetAllCardName()
    {
        string sOutput = "";
        for(int i = 0; i < base.Cards.Count; i++)
        {
            sOutput += base.Cards[i].CardName;
            if(i+1 < base.Cards.Count) { sOutput += "/"; }
        }
        return sOutput;
    }
    /// <summary>
    /// 抽牌function，一次一張
    /// </summary>
    /// <param name="_iIndex">從何抽起</param>
    /// <returns></returns>
    public ICard DrawCard(int _iIndex = 0)
    {
        ICard cardTemp = null;
        if(base.Cards.Count > 0)
        {
            if (_iIndex < 0) { _iIndex = 0; }
            else if (_iIndex >= base.Cards.Count) { _iIndex = base.Cards.Count - 1; }
            cardTemp = base.Cards[_iIndex];
            base.Cards.RemoveAt(_iIndex);
        }        
        return cardTemp;
    }
}