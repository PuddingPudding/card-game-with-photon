﻿using UnityEngine;

public class Card : ICard
{
    public virtual string CardName { get; protected set; }   //你可能不會卡牌直接有自己的名字，而是呼叫時會去圖鑑裡回傳名字

    public Card(string _sCardName)
    {
        this.CardName = _sCardName;
    }

    /// <summary>
    /// 另外再回傳一個卡牌的實體 (deep clone)
    /// </summary>
    /// <returns></returns>
    public virtual Card Clone()
    {
        return new Card(this.CardName);
    }

    ICard ICard.Clone()
    {
        throw new System.NotImplementedException();
    }
}
//public class HanamikojiGift : Card
//{
//    public HanamikojiGift(string _sCardName) : base(_sCardName)
//    {
//    }
//}
public interface ICard
{
    string CardName { get; }
    ICard Clone();
}
/*GiftCard (藝妓禮物卡?)
* 對應藝妓 (送禮對象，預設是固定的，之後可能會有可變化的禮物牌)
* 好感度分數 (基本上都是1，之後也許會有變體)
*/
public class HanamikojiGift : ICard
{
    public string CardName
    {
        get
        {
            string outputStr = "none";
            switch (this.GeishaType)
            {
                case EGeishaType.FLUTE:
                    outputStr = "日式篠笛";
                    break;
                case EGeishaType.SCROLL:
                    outputStr = "古書卷軸";
                    break;
                case EGeishaType.FAN:
                    outputStr = "舞扇";
                    break;
                case EGeishaType.UMBRELLA:
                    outputStr = "油紙傘";
                    break;
                case EGeishaType.SHAMISEN:
                    outputStr = "三味線";
                    break;
                case EGeishaType.CUPS:
                    outputStr = "茶具";
                    break;
                case EGeishaType.HAIRPIN:
                    outputStr = "櫻花髮簪";
                    break;
            }
            return outputStr;
        }
    }
    /// <summary>
    /// 提供的好感度 (預設為1)
    /// </summary>
    public float Favorability = 1;
    public EGeishaType GeishaType { get; protected set; }

    public HanamikojiGift(EGeishaType _type = EGeishaType.FLUTE)
    {
        this.Favorability = 1;
        this.GeishaType = _type;
    }

    public ICard Clone()
    {
        return new HanamikojiGift(this.GeishaType);
    }
}

