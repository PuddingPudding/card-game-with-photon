﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Hanamikoji/BasicCardData")]
public class HanamikojiBasicCardData : ScriptableObject
{
    [SerializeField]
    private int m_iTotal = 2;
    [SerializeField]
    private EGeishaType m_type = EGeishaType.FLUTE;

    /// <summary>
    /// 獲得該卡牌在一套遊戲中含有幾張的資訊
    /// </summary>
    public int TotalNum { get { return this.m_iTotal; } }
    /// <summary>
    /// 獲取該卡牌是送給哪個藝妓的資訊(不過這邊是統一都用禮物在記)
    /// </summary>
    public EGeishaType GeishaType { get { return m_type; } }
}