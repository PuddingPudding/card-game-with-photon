﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickStartLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Button m_quickStartBtn;
    [SerializeField]
    private Button m_quickCancelBtn;
    [SerializeField]
    private GameObject m_loadingBar;
    [SerializeField]
    private int m_iRoomSize;

    private void Start()
    {
        this.m_quickStartBtn.gameObject.SetActive(false);
        this.m_quickStartBtn.onClick.AddListener(this.QuickStart);
        this.m_quickCancelBtn.gameObject.SetActive(false);
        this.m_quickCancelBtn.onClick.AddListener(this.QuickCancel);
    }

    public override void OnConnectedToMaster() //確認連到伺服器後，開啟快速加入的按鈕
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        //定義說連進同個房間的大家是否應該都要在同一個場景
        m_quickStartBtn.gameObject.SetActive(true);
        this.m_loadingBar.SetActive(false);
    }

    public void QuickStart()
    {
        m_quickStartBtn.gameObject.SetActive(false);
        m_quickCancelBtn.gameObject.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("Quick Start");
    }

    public override void OnJoinRandomFailed(short _returnCode, string _sMsg)
    {
        Debug.Log("加入隨機房間失敗 (估計沒有房間)");
        this.CreateRoom();
    }

    void CreateRoom()
    {
        Debug.Log("自行建立房間中");
        int iRandomRoom = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)m_iRoomSize };
        PhotonNetwork.CreateRoom("Room" + iRandomRoom, roomOps);
        Debug.Log("新隨機房間號碼" + iRandomRoom);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("建立房間失敗，資訊:" + message);
        this.CreateRoom(); //可能因為名字重複而失敗，試著再建立一次
    }

    public void QuickCancel()
    {
        m_quickCancelBtn.gameObject.SetActive(false);
        m_quickStartBtn.gameObject.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
