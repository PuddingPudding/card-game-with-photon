﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PhotonPlayerMovement : MonoBehaviourPun
{
    [SerializeField]
    private float m_fMovementSpped = 2;
    [SerializeField]
    private TextMeshProUGUI m_idTxt;

    // Start is called before the first frame update
    void Start()
    {
        //m_idTxt.text = PhotonNetwork.NickName; //取得你自己的綽號，當你調整該數值時，房內的其他人看你也會跟著改動
        m_idTxt.text = this.photonView.Owner.NickName;//依照PhotonView直接取得該物件持有人的綽號
        if (this.photonView.IsMine)
        {
            m_idTxt.color = Color.white;
        }
        else
        {
            m_idTxt.color = Color.yellow;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(this.photonView.IsMine)
        {
            Vector3 finalVector = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
            {
                finalVector += new Vector3(0, 1, 0);
            }
            if (Input.GetKey(KeyCode.S))
            {
                finalVector += new Vector3(0, -1, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {
                finalVector += new Vector3(-1, 0, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                finalVector += new Vector3(1, 0, 0);
            }
            finalVector = finalVector.normalized * m_fMovementSpped;
            this.transform.position += finalVector * Time.deltaTime;
        }        
    }
}
