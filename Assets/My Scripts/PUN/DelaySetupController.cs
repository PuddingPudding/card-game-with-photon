﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum EWaitingRoomStatus
{
    WAITING,
    READY,
    STARTING_GAME
}

public class DelaySetupController : MonoBehaviourPunCallbacks
{
    private PhotonView m_photonView;
    //PhotonView定義了一個在網路上的實體，讓連到同伺服器的人可以辨識，並且讓客戶端可以遠端去操作

    /* 此處目標
     * 當房間內玩家不足最低玩家需求時->等待
     * 當到達最低限度時->等普通時間後開始遊戲(預計10秒)
     * 滿玩家時等待最短的時間開始遊戲 (預計5秒)
    */

    [SerializeField]
    private EWaitingRoomStatus m_roomStatus;
    /// <summary>
    /// 實際會去多人遊戲的場景(SceneIndex)
    /// </summary>
    [SerializeField]
    private int m_iMultiplayerSceneIndex;
    /// <summary>
    /// 回主選單的場景
    /// </summary>
    [SerializeField]
    private int m_iMenuSceneIndex;
    /// <summary>
    /// 遊戲開始的最低人數限制 (以這個練習專案來說的話為2人，再多上去邏輯上可處理，但畫面上我想不太到要怎麼用)
    /// </summary>
    [SerializeField]
    private int m_iMinPlayerToStart;
    /// <summary>
    /// 顯示現在房內有幾個玩家的文字
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI m_roomCountTxt;
    /// <summary>
    /// 顯示再過多久開始遊戲的文字
    /// </summary>
    [SerializeField]
    private TextMeshProUGUI m_timerTxt;
    [SerializeField]
    private float m_fMaxWaitTime;
    [SerializeField]
    private float m_fFullRoomWaitTime;

    private float m_fCountdownTimer;

    private int m_iPlayerCount;
    private int m_iRoomSize;

    private void Start()
    {
        m_photonView = this.GetComponent<PhotonView>();

        this.ResetTimer();

        this.PlayerCountUpdate();
    }

    void PlayerCountUpdate()
    {
        m_iPlayerCount = PhotonNetwork.PlayerList.Length;
        m_iRoomSize = PhotonNetwork.CurrentRoom.MaxPlayers;
        m_roomCountTxt.text = m_iPlayerCount + "/" + m_iRoomSize;

        if(m_iPlayerCount >= m_iMinPlayerToStart)
        {
            this.m_roomStatus = EWaitingRoomStatus.READY;
            if(m_iPlayerCount == m_iRoomSize && m_fCountdownTimer > m_fFullRoomWaitTime)
            {
                m_fCountdownTimer = m_fFullRoomWaitTime;
            }
        }
        else
        {
            this.m_roomStatus = EWaitingRoomStatus.WAITING;
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        this.PlayerCountUpdate();
        if (PhotonNetwork.IsMasterClient)
        {
            m_photonView.RPC("RPC_SendTimer", RpcTarget.Others, m_fCountdownTimer);
            //廣播，叫除了自己以外的玩家都執行一次SendTimer
        }
    }

    [PunRPC]
    private void RPC_SendTimer(float _fTimeIn)
    {
        m_fCountdownTimer = _fTimeIn;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        this.PlayerCountUpdate();
    }

    private void Update()
    {
        this.WaitingForMorePlayers();
    }

    void WaitingForMorePlayers()
    {
        if (m_iPlayerCount < m_iMinPlayerToStart)
        {
            ResetTimer();
        }
        else if(m_roomStatus == EWaitingRoomStatus.READY)
        {
            m_fCountdownTimer -= Time.deltaTime;
        }
       
        string sTempTimer = string.Format("{0:00}", m_fCountdownTimer);
        m_timerTxt.text = sTempTimer;

        if ( m_fCountdownTimer <= 0)
        {
            if (this.m_roomStatus != EWaitingRoomStatus.STARTING_GAME)
            {
                this.StartGame();
            }
        }
    }

    void ResetTimer()
    {
        m_fCountdownTimer = m_fMaxWaitTime;
    }

    void StartGame()
    {
        m_roomStatus = EWaitingRoomStatus.STARTING_GAME;

        //m_bStartingGame = true;
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            //如果配對完成，開始遊玩的時候把房間鎖起來 (?
            PhotonNetwork.LoadLevel(m_iMultiplayerSceneIndex);
        }
    }

    public void DelayCancel()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(m_iMenuSceneIndex);
    }
}
