﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUNhanamikojiGM : MonoBehaviourPun
{
    /*
     * 只有Master端會先生成GM->初始化牌堆，再用廣播的方式讓其他客戶端看到的牌堆資訊同步
     * 目前是都只有資料更新，顯示層依據其更新內容調動
     * 
     */

    /// <summary>
    /// 牌堆
    /// </summary>
    private Deck m_hanamikojiDeck;
    private HanamikojiGM m_gm;
    /// <summary>
    /// 全部的藝妓 (通常為7個)
    /// </summary>
    public List<Geisha> AllGeishas { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            m_gm.InitHanamikojiDeck();
        }
    }

    /// <summary>
    /// 更新牌堆資訊
    /// </summary>
    [PunRPC]
    void UpdateDeck(Deck _deck)
    {
        //複製牌堆資訊
        this.m_hanamikojiDeck.CopyCards(_deck.Cards);
    }
    /// <summary>
    /// 在主玩家那邊的GM資訊更新時，廣播覺其他玩家跟著同步
    /// </summary>
    /// <param name="_GM"></param>
    [PunRPC]
    void UpdateGM(HanamikojiGM _GM)
    {
        this.m_gm = _GM;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
