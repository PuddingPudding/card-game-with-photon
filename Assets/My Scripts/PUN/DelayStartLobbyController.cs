﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelayStartLobbyController : MonoBehaviourPunCallbacks
{ //乖乖照著打才發現這個Class功能有點重複了
    [SerializeField]
    private Button m_delayStartBtn;
    [SerializeField]
    private Button m_delayCancelBtn;
    [SerializeField]
    private int m_iRoomSize;

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        m_delayStartBtn.gameObject.SetActive(true);
    }

    public void DelayStart()
    {
        m_delayStartBtn.gameObject.SetActive(false);
        m_delayCancelBtn.gameObject.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("Delay Start");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        this.CreateRoom();
    }

    void CreateRoom()
    {
        Debug.Log("Creating room now");
        int iRandomRoom = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)m_iRoomSize };
        PhotonNetwork.CreateRoom("Room" + iRandomRoom, roomOps);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("建立房間失敗，資訊:" + message);
        this.CreateRoom(); //可能因為名字重複而失敗，試著再建立一次
    }

    public void QuickCancel()
    {
        m_delayCancelBtn.gameObject.SetActive(false);
        m_delayStartBtn.gameObject.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }
}
