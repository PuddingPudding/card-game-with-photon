﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomButton : MonoBehaviour
{
    [SerializeField]
    private Text m_nameTxt;
    [SerializeField]
    private Text m_sizeTxt;

    private string m_sRoomName;
    private int m_iRoomSize;
    private int m_iPlayerCount;

    public void JoinRoomOnClick()
    {
        PhotonNetwork.JoinRoom(m_sRoomName);
    }
    public void SetRoom(string _sNameInput , int _iSizeInput , int _iCountInput)
    {
        m_sRoomName = _sNameInput;
        m_iRoomSize = _iSizeInput;
        m_iPlayerCount = _iCountInput;
        m_nameTxt.text = _sNameInput;
        m_sizeTxt.text = _iCountInput + "/" + _iSizeInput;
    }
}
