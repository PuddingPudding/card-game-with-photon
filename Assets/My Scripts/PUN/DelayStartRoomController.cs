﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DelayStartRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int m_iWaitingRoomSceneIndex;

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }
    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override void OnJoinedRoom() //偵測到進入了房間(創立房間也會走過這條判定)
    {
        Debug.Log("已進入房間");
        SceneManager.LoadScene(m_iWaitingRoomSceneIndex);
    }
}
