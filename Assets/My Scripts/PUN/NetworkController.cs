﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkController : MonoBehaviourPunCallbacks
{
    /*Photon Network API文件
     * https://doc-api.photonengine.com/en/pun/v2/class_photon_1_1_pun_1_1_photon_network.html#a3eb35e234e79134ae83b98d35a86c317
     * 
     */

    [SerializeField]
    private GameObject m_loadingBar;

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
        //透過你的PhotonServerSettings去連線

        m_loadingBar.SetActive(true);
    }

    public override void OnConnectedToMaster() //連到伺服器時呼叫
    {
        Debug.Log("我們連接到了" + PhotonNetwork.CloudRegion + "伺服器");
        //如果你不是用fixed region(固定地區)的話，它會自動去找最佳的區域
        m_loadingBar.SetActive(false);
    }
}
