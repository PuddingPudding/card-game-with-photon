﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomMatchMakingRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int m_iMultiPlayerSceneIndex;

    [SerializeField]
    private GameObject m_lobbyPanel;
    [SerializeField]
    private GameObject m_roomPanel;
    [SerializeField]
    private Button m_startBtn;

    /// <summary>
    /// 乘載玩家資訊的物件 (顯示上)
    /// </summary>
    [SerializeField]
    private Transform m_playersContainer;
    [SerializeField]
    private GameObject m_playerListingPrefab;

    [SerializeField]
    private TextMeshProUGUI m_roomNameDisplayTxt;
    [SerializeField]
    private TextMeshProUGUI m_roomPlayerNumTxt;

    [SerializeField]
    private Vector2 m_playerListingStartPos;
    [SerializeField]
    private Vector2 m_playerListingInterval;

    void ClearPlayerListings()
    {
        for(int i = m_playersContainer.childCount-1; i>=0; i--)
        {
            Destroy(m_playersContainer.GetChild(i).gameObject);
        }
    }
    /// <summary>
    /// 根據PhotonNetwork提供的玩家列表資料刷新畫面
    /// </summary>
    void ListPlayers()
    {
        for(int i = 0; i < PhotonNetwork.PlayerList.Length ; i++)
        {
            GameObject tempListing = Instantiate(m_playerListingPrefab, m_playersContainer);
            tempListing.transform.localPosition = m_playerListingStartPos + (i * m_playerListingInterval);
            Text textTemp = tempListing.transform.GetChild(0).GetComponent<Text>();
            textTemp.text = PhotonNetwork.PlayerList[i].NickName;
        }
        m_roomPlayerNumTxt.text = PhotonNetwork.PlayerList.Length + "/" + PhotonNetwork.CurrentRoom.MaxPlayers ;
    }
    public override void OnJoinedRoom()
    {
        m_roomPanel.SetActive(true);
        m_lobbyPanel.SetActive(false);
        m_roomNameDisplayTxt.text = PhotonNetwork.CurrentRoom.Name;
        if (PhotonNetwork.IsMasterClient) { m_startBtn.gameObject.SetActive(true);  }
        else { m_startBtn.gameObject.SetActive(false); }

        this.ClearPlayerListings();
        this.ListPlayers();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        this.ClearPlayerListings();
        this.ListPlayers();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        this.ClearPlayerListings();
        this.ListPlayers();
        if(PhotonNetwork.IsMasterClient)
        {
            m_startBtn.gameObject.SetActive(true);
        }
    }

    public void StarGame()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.LoadLevel(m_iMultiPlayerSceneIndex);
        }
    }

    IEnumerator RejoinLobby(float _fDelay = 1)
    {
        yield return new WaitForSeconds(_fDelay);
        PhotonNetwork.JoinLobby();
    }

    public void BackOnClick()
    {
        m_lobbyPanel.SetActive(true);
        m_roomPanel.SetActive(false);
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LeaveLobby();
        StartCoroutine(this.RejoinLobby(2));
    }
}
