﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomMatchMakingLobbyController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private Button m_lobbyConnectBtn;
    [SerializeField]
    private GameObject m_lobbyPanel;
    [SerializeField]
    private GameObject m_mainPanel;
    [SerializeField]
    private TMP_InputField m_playerNameInput;
        
    private string m_sRoomName;
    /// <summary>
    /// 房間大小暫且固定為2
    /// </summary>
    private int m_iRoomSize = 2;
    /// <summary>
    /// "房間資訊"的列表
    /// </summary>
    private List<RoomInfo> m_listRoomInfos;

    [SerializeField]
    private Transform m_roomsContainer;
    [SerializeField]
    private GameObject m_roomListingPrefab;
    [SerializeField]
    private List<RoomButton> m_listRoomBtn;
    [SerializeField]
    private Vector2 m_roomListingStartPos;
    [SerializeField]
    private Vector2 m_roomListingInterval;

    private void Start()
    {
        m_lobbyConnectBtn.gameObject.SetActive(false);
        m_playerNameInput.gameObject.SetActive(false);
        m_lobbyPanel.SetActive(false);
        m_listRoomInfos = new List<RoomInfo>();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        m_lobbyConnectBtn.gameObject.SetActive(true);
        m_playerNameInput.gameObject.SetActive(true);
        m_listRoomInfos.Clear();

        //若本地端已經存有NickName資訊
        if (PlayerPrefs.HasKey("NickName"))
        {
            if (PlayerPrefs.GetString("NickName") == "")
            {
                PhotonNetwork.NickName = "Player" + UnityEngine.Random.Range(0, 1000);
            }
            else
            {
                PhotonNetwork.NickName = PlayerPrefs.GetString("NickName");
            }
            m_playerNameInput.text = PhotonNetwork.NickName;
        }
    }
    /// <summary>
    /// 當玩家名稱更動，主要是給m_playerNameInput(輸入欄)呼叫的
    /// </summary>
    /// <param name="_sName"></param>
    public void PlayerNameUpdate(string _sName)
    {
        Debug.Log("玩家名稱:" + _sName);
        PhotonNetwork.NickName = _sName;
        PlayerPrefs.SetString("NickName", _sName);
        m_playerNameInput.text = _sName;
    }

    /// <summary>
    /// 切換顯示的UI群並進入大廳
    /// </summary>
    public void JoinLobbyOnClick()
    {
        m_mainPanel.SetActive(false);
        m_lobbyPanel.SetActive(true);
        PhotonNetwork.JoinLobby();
        //Lobby在Photon當中算是裝房間(Room)的列表，使用JoinLobby會進到伺服器剛開始提供的預設Lobby
    }

    /// <summary>
    /// 每當Room列表有所變動時，過來執行更新，特別注意，這邊只會去接收有"變動"的房間資訊
    /// ，例如新增了一個房間，那就只會去通報新的那個房間資訊而已
    /// </summary>
    /// <param name="_roomList"></param>
    public override void OnRoomListUpdate(List<RoomInfo> _roomList)
    {//另外，當你已經加入到任意房內的時候就不會再通知你了
     /*
    * m_listRoomInfos需去紀錄所有房間的資訊 (因為OnRoomListUpdate不會提供所有房間的資訊)
    * 在離開房間的時候去清空，以便之後刷新房間列表的資訊 
    * (注意:若退房後想要獲得完整房間列表資訊，你會需要先PhotonNetwork.LeaveLobby()，然而OnLeftLobby好像沒有反應)
    * 需要:檢測是否已存在於m_list房間資訊中，若存在，檢查該房間是否還有效
    */

        Debug.Log("房間更新");
        int iIndexTemp;
        for (int i = 0; i < _roomList.Count; i++)
        {
            Debug.Log(_roomList[i].Name);
            //this.ListRoom(_roomList[i]);
            iIndexTemp = m_listRoomInfos.FindIndex(ByName(_roomList[i].Name));
            if (iIndexTemp >= 0) //若該房間目前已經存在於我們自己的列表當中
            {
                if (_roomList[i].PlayerCount <= 0 || !_roomList[i].IsOpen)
                {
                    m_listRoomInfos.RemoveAt(iIndexTemp);
                }
                else
                {
                    m_listRoomInfos[iIndexTemp] = _roomList[i];
                }
            }
            else if (_roomList[i].PlayerCount > 0 && _roomList[i].IsOpen)
            {//假如不存在於原列表，且符合資格(房內有玩家，且屬於開放的房間)
                m_listRoomInfos.Add(_roomList[i]);
            }
            this.RefreshRoomListing();

            #region 原本教學影片裡的RoomListUpdate
            //if (m_listRoomInfos != null)
            //{
            //    iIndexTemp = m_listRoomInfos.FindIndex(ByName(_roomList[i].Name));
            //    //找出第一個名字跟輸入值一樣的索引值
            //}
            //else
            //{
            //    iIndexTemp = -1;
            //}

            //if(iIndexTemp != -1) //若為有相同名稱的房間的話，刪除舊有的房間 (?
            //{ //我看不太懂原作者在寫三小
            //    Debug.Log("已刪除……吧");
            //    m_listRoomInfos.RemoveAt(iIndexTemp);
            //    Destroy(m_roomsContainer.GetChild(iIndexTemp).gameObject);
            //}
            //if(_roomList[i].PlayerCount > 0)
            //{//若輸入進來的房間內玩家數量>0
            //    m_listRoomInfos.Add(_roomList[i]);
            //    this.ListRoom(_roomList[i]);
            //}
            #endregion 原本教學影片裡的RoomListUpdate
        }
    }
    public override void OnJoinedLobby()
    {
        Debug.Log("進入大廳");
    }
    public override void OnLeftLobby()
    {
        Debug.Log("離開大廳");
    }
    public override void OnLeftRoom()
    {
        Debug.Log("離開房間，刷新房間列表資訊");
        this.m_listRoomInfos.Clear();
    }
    /// <summary>
    /// Predicate有點像是定義一個準則，而你可以在List當中直接去找到符合該準則的物件
    /// </summary>
    /// <param name="_sName"></param>
    /// <returns></returns>
    static System.Predicate<RoomInfo> ByName(string _sName)
    {
        return delegate (RoomInfo _room)
        {
            return _room.Name == _sName;
        };
    }

    void ListRoom(RoomInfo _room)
    {
        if (_room.IsOpen && _room.IsVisible)
        {
            /*問題點
             * 如果今天進B房以前我們先看到了A房生成的消息，在我們離開B房前，A房以解散
             * 接著我們再退出B房，由於沒有看到A房解散的消息，因此我們列表上的A房會繼續保留
             */
            Debug.Log("設GG房間");
            GameObject tempListing = Instantiate(m_roomListingPrefab, m_roomsContainer);
            RoomButton btnTemp = tempListing.GetComponent<RoomButton>();
            btnTemp.SetRoom(_room.Name, _room.MaxPlayers, _room.PlayerCount);
        }
    }
    void RefreshRoomListing()
    {
        for (int i = 0; i < m_listRoomInfos.Count || i < m_listRoomBtn.Count; i++)
        {
            if (i < m_listRoomInfos.Count)
            {
                if (i >= m_listRoomBtn.Count)
                {
                    GameObject tempListing = Instantiate(m_roomListingPrefab, m_roomsContainer);
                    tempListing.transform.localPosition = m_roomListingStartPos + (i * m_roomListingInterval);
                    m_listRoomBtn.Add(tempListing.GetComponent<RoomButton>());
                }
                m_listRoomBtn[i].SetRoom(m_listRoomInfos[i].Name, m_listRoomInfos[i].MaxPlayers, m_listRoomInfos[i].PlayerCount);
                m_listRoomBtn[i].gameObject.SetActive(true);
            }
            else
            {
                m_listRoomBtn[i].gameObject.SetActive(false);
            }
        }
    }

    #region 主要給InputField互動用
    public void OnRoomNameChanged(string _sName)
    {
        this.m_sRoomName = _sName;
    }
    public void OnRoomSizeChanged(string _sSizeInput)
    {
        Debug.Log("房間大小輸入值:" + _sSizeInput);
        this.m_iRoomSize = Int32.Parse(_sSizeInput);
    }
    #endregion 主要給InputField互動用

    /// <summary>
    /// 依照自己的m_sRoomName和m_iRoomSize去建立一個新房間
    /// </summary>
    public void CreateRoom()
    {
        Debug.Log("Creating room now");
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)m_iRoomSize };
        PhotonNetwork.CreateRoom(m_sRoomName, roomOps);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Create room failed.");
    }

    /// <summary>
    /// 切換回主UI，關閉LobbyUI並退出Lobby
    /// </summary>
    public void MatchmakingCancel()
    {
        m_mainPanel.SetActive(true);
        m_lobbyPanel.SetActive(false);
        PhotonNetwork.LeaveLobby();
    }
}
