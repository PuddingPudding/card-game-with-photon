﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 藝妓類別 (這裡為了方便記錄，就直接以他們喜歡的禮物為名)
/// </summary>
public enum EGeishaType
{
    FLUTE = 0,
    SCROLL,
    FAN,
    UMBRELLA,
    SHAMISEN,
    CUPS,
    HAIRPIN
}

/*Geisha (藝妓需要什麼資料?)
 * 她現在收到哪位玩家幾個禮物? (這部分感覺改為玩家身上紀錄 "我送給了哪位藝妓幾個禮物" 會比較容易)
 * 或著讓她直接身上帶有int列表，初始化時會去對應GM目前有幾個玩家來建立列表
 * (不過int列表比較大的問題在於說我們不知道之後會不會加入特殊的禮物卡，所以應該還是用Card列表來處理比較好)
 * 她現在傾向哪位玩家 (一個int，對應到第幾號玩家)，結算時若平手(沒有禮物也算)，則會直接投靠傾向的玩家
 */
public class Geisha
{
    /// <summary>
    /// 對應到每個玩家現在擁有的好感度
    /// </summary>
    public List<float> PlayersFavorability;
    /// <summary>
    /// 較喜歡的玩家(編號，若為負數則表示沒有)，較喜歡的玩家可以在平手時直接被記為歸順該玩家
    /// </summary>
    public int PlayerLiked;
    public float CharmPoint;
    public EGeishaType Type;
    public static float[] CharmPointsTable = new float[] { 2, 2, 2, 3, 3, 4, 5 };

    public Geisha(List<float> _fListPlayersFavorability, float _fCharmPoint, EGeishaType _type = EGeishaType.FLUTE)
    {
        this.PlayerLiked = -1;
        this.PlayersFavorability = new List<float>();
        for (int i = 0; i < _fListPlayersFavorability.Count; i++) { this.PlayersFavorability.Add(_fListPlayersFavorability[i]); }
        //this.PlayersFavorability = _fListPlayersFavorability; //shallow copy
        this.CharmPoint = _fCharmPoint;
        this.Type = _type;
    }
    public Geisha(List<float> _fListPlayersFavorability, EGeishaType _type)
    {
        this.PlayerLiked = -1;
        this.PlayersFavorability = new List<float>();
        for (int i = 0; i < _fListPlayersFavorability.Count; i++) { this.PlayersFavorability.Add(_fListPlayersFavorability[i]); }
        this.Type = _type;
        this.CharmPoint = Geisha.CharmPointsTable[(int)_type];
    }
    public override string ToString()
    {
        string sFavorability = "[";
        for (int i = 0; i < this.PlayersFavorability.Count; i++)
        {
            sFavorability += this.PlayersFavorability[i];
            if (i + 1 < this.PlayersFavorability.Count) { sFavorability += ","; }
        }
        sFavorability += "]";
        return this.Type + "/魅力:" + this.CharmPoint + "/好感度:" + sFavorability;
    }
}