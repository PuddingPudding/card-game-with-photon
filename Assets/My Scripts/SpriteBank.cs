﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Simple Sprite Bank")]
public class SpriteBank : ScriptableObject
{
    [SerializeField]
    private Sprite[] m_arrSprite;

    public Sprite[] Sprites { get { return this.m_arrSprite; } }
}
