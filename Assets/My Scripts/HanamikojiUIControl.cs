﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HanamikojiUIControl : MonoBehaviour
{
    public SimpleBtn m_btnPrefab;
    public Text m_hanInfoTxt;

    [SerializeField]
    private List<SimpleBtn> m_listCards;
    [SerializeField]
    private List<SimpleBtn> m_listCardsFromOpponent;
    [SerializeField]
    private Color m_normalCardClr = Color.white;
    [SerializeField]
    private Color m_chosenCardClr = Color.yellow;
    [SerializeField]
    private Color m_chosenActionClr = Color.green;
    [SerializeField]
    private Vector2 m_btnSpawnBasePos = new Vector2(0, -80);
    [SerializeField]
    private Vector2 m_btnInterval = new Vector2(60, 0);
    [SerializeField]
    private Vector2 m_responseInterval = new Vector2(90, 0);
    [SerializeField]
    private Vector2 m_btnStackInterval = new Vector2(20, -20);
    [SerializeField]
    private List<Button> m_listActionBtn;
    [SerializeField]
    private Button m_confirmBtn;
    [SerializeField]
    private TextMeshProUGUI m_playerNameTxt;
    [SerializeField]
    private Text m_hintTxt;
    [SerializeField]
    private GameObject m_opponentCardsPanel;
    [SerializeField]
    private Vector2 m_opponentCardsPanelWorldPos = new Vector2(0, 0);
    [SerializeField]
    private Button m_opponentCardsPanelSwitch;
    /// <summary>
    /// 負責觀察的玩家編號
    /// </summary>
    [SerializeField]
    private int m_iObservingPlayer = 0;
    [SerializeField]
    private HanamikojiGM m_observingGM;
    //2020/3/24 未來可能不要讓View直接看到GM，而是交由某個GameStarter來分配
    private HanamikojiController m_controller;
    private int m_iCurrentCardsNeed = 1;

    private void Start()
    {
        m_observingGM.PlayerUpdateCallback += this.OverallUpdate;
        this.m_controller = m_observingGM.AllControllers[m_iObservingPlayer];
        this.m_playerNameTxt.text = m_iObservingPlayer + "P";
        this.OverallUpdate(m_iObservingPlayer, m_observingGM.AllPlayers[m_iObservingPlayer]);
        int iActionNum = Enum.GetNames(typeof(EHanamikojiAction)).Length;
        for (int i = 0; i < iActionNum && i < m_listActionBtn.Count; i++)
        {
            int iTemp = i;
            m_listActionBtn[iTemp].onClick.AddListener(() => { this.TryToChooseAction((EHanamikojiAction)iTemp); });
        }
        this.m_confirmBtn.onClick.AddListener(() => { this.TryToConfirm(); });

        this.RefreshHandClr();
        //this.m_opponentCardsPanel.transform.position = this.m_opponentCardsPanelWorldPos; 
        //雖然要用到一致的位置，但是對UI直接這樣設定的話會跑到最邊邊
        this.m_opponentCardsPanel.SetActive(false);
        this.m_opponentCardsPanelSwitch.gameObject.SetActive(false);
        this.m_opponentCardsPanelSwitch.onClick.AddListener(this.OpponentCardsDisplaySwitch);
    }
    /// <summary>
    /// 整體更新 (但是只有在輸入值是你負責觀察的玩家編號時更新)
    /// </summary>
    /// <param name="_iPlayerIndex"></param>
    /// <param name="_player"></param>
    private void OverallUpdate(int _iPlayerIndex, HanamikojiPlayer _player)
    {
        if (this.m_controller.IsCurrentPlayer) { this.TryToChooseAction(m_controller.m_currentAction); }
        else{ this.m_hintTxt.text = "等待中"; }
        if (_iPlayerIndex == this.m_iObservingPlayer)
        {
            this.UpdatePlayerActionView(_player);
            this.UpdateHandsView(_player);
            this.UpdateOpponentCardsView(_player);
        }
    }

    public void UpdatePlayerActionView(HanamikojiPlayer _player)
    {
        int iActionNum = Enum.GetNames(typeof(EHanamikojiAction)).Length;
        for (int i = 0; i < iActionNum && i < m_listActionBtn.Count; i++)
        {
            m_listActionBtn[i].interactable = _player.CheckActionLeft((EHanamikojiAction)i);
        }
    }

    /// <summary>
    /// 根據玩家手牌資料更新顯示(只接收自己負責觀察的那一號玩家)
    /// </summary>
    /// <param name="_iPlayerIndex"></param>
    /// <param name="_player"></param>
    public void UpdateHandsView(int _iPlayerIndex, HanamikojiPlayer _player)
    {
        if (_iPlayerIndex == this.m_iObservingPlayer)
        {
            this.UpdateHandsView(_player);
        }
    }
    /// <summary>
    /// 根據玩家手牌資料更新顯示
    /// </summary>
    public void UpdateHandsView(HanamikojiPlayer _player)
    {
        int iHandSize = _player.Cards.Count;
        Vector2 btnStartPos = this.m_btnSpawnBasePos - (m_btnInterval * ((iHandSize - 1) / 2.0f));
        Vector2 targetPos = Vector2.zero;
        for (int i = 0; i < m_listCards.Count || i < iHandSize; i++)
        {
            if (i >= this.m_listCards.Count)
            {
                SimpleBtn btnTemp = GameObject.Instantiate(m_btnPrefab);
                btnTemp.gameObject.SetActive(false);
                this.m_listCards.Add(btnTemp);
                btnTemp.transform.SetParent(this.transform);
                int iTemp = i;
                btnTemp.btn.onClick.AddListener(() => { this.TryToChooseCard(iTemp); });
            }

            targetPos = btnStartPos + (m_btnInterval * i);
            if (i < iHandSize)
            {
                this.m_listCards[i].transform.localPosition = targetPos;
                //this.m_listCards[i].txt.text = "" + i;
                HanamikojiGift giftCardTemp = (HanamikojiGift)_player.Cards[i];
                this.m_listCards[i].SetHanamikojiGiftFace(giftCardTemp.GeishaType);
                this.m_listCards[i].gameObject.SetActive(true);
            }
            else
            {
                this.m_listCards[i].gameObject.SetActive(false);
            }
        }
        this.m_hanInfoTxt.text = _player.GetStatus();
    }
    /// <summary>
    /// 偵測說如果有敵人打到面前的牌，要如何顯示
    /// </summary>
    /// <param name="_player"></param>
    private void UpdateOpponentCardsView(HanamikojiPlayer _player)
    {
        if (_player.CardsFromOpponent != null)
        {
            this.m_opponentCardsPanel.SetActive(true);
            this.m_opponentCardsPanelSwitch.gameObject.SetActive(true);
            Vector2 localSpawnStartPos = Vector2.zero;
            Debug.Log("玩家" + m_iObservingPlayer + "現在接收到了Action" + _player.OpponentAction);
            //競爭時，卡牌為兩兩一組看待
            if (_player.OpponentAction == EHanamikojiAction.COMPETITION)
            {
                localSpawnStartPos -= (((_player.CardsFromOpponent.Count / 2 - 1) / 2.0f) * m_responseInterval);
            }
            else { localSpawnStartPos -= (((_player.CardsFromOpponent.Count - 1) / 2.0f) * m_responseInterval); }
            for (int i = 0; i < _player.CardsFromOpponent.Count || i < this.m_listCardsFromOpponent.Count; i++)
            {
                int iTemp = i; //為了在跑迴圈的過程中成功指派索引值而設定
                if (i >= this.m_listCardsFromOpponent.Count)
                {
                    SimpleBtn cardTemp = GameObject.Instantiate(this.m_btnPrefab);
                    cardTemp.transform.SetParent(m_opponentCardsPanel.transform);
                    this.m_listCardsFromOpponent.Add(cardTemp);
                }
                if (i < _player.CardsFromOpponent.Count)
                {
                    this.m_listCardsFromOpponent[i].SetHanamikojiGiftFace(_player.CardsFromOpponent[i].GeishaType);
                    this.m_listCardsFromOpponent[i].btn.onClick.RemoveAllListeners();
                    //"競爭"的排法較特殊
                    if (_player.OpponentAction == EHanamikojiAction.COMPETITION)
                    {
                        Vector2 cardFinalPos = localSpawnStartPos + (m_responseInterval * (i / 2));
                        cardFinalPos += (m_btnStackInterval) * ((i % 2) - 0.5f);
                        this.m_listCardsFromOpponent[i].transform.localPosition = cardFinalPos;
                        if (i % 2 == 0) { this.m_listCardsFromOpponent[i].btn.onClick.AddListener(() => this.TryToRespondOpponentAction(iTemp)); }
                    }
                    else
                    {
                        this.m_listCardsFromOpponent[i].transform.localPosition = localSpawnStartPos + (m_responseInterval * i);
                        this.m_listCardsFromOpponent[i].btn.onClick.AddListener(() => this.TryToRespondOpponentAction(iTemp));
                    }
                    this.m_listCardsFromOpponent[i].gameObject.SetActive(true);
                }
                else
                {
                    this.m_listCardsFromOpponent[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            this.m_opponentCardsPanel.SetActive(false);
            this.m_opponentCardsPanelSwitch.gameObject.SetActive(false);
        }
    }
    private void OpponentCardsDisplaySwitch()
    {
        this.m_opponentCardsPanel.SetActive(!m_opponentCardsPanel.activeInHierarchy);
    }
    /// <summary>
    /// 試著去回應對手的甩到面前的卡牌
    /// </summary>
    /// <param name="_iIndex"></param>
    private void TryToRespondOpponentAction(int _iIndex)
    {
        bool bSuccess = this.m_controller.ResponseOpponent(_iIndex);
        if (bSuccess)
        {
            this.m_opponentCardsPanel.SetActive(false);
            this.m_opponentCardsPanelSwitch.gameObject.SetActive(false);
        }
    }

    private void TryToConfirm()
    {
        if (this.m_controller.IsCurrentPlayer)
        {
            bool bSuccess = this.m_controller.InvokeHanamikojiAction();
            if (!bSuccess)
            {
                this.m_hintTxt.text = "錯誤，請挑出" + m_iCurrentCardsNeed + "張手牌";
                string sErrorOutput = "卡牌數量對不上(Action:" + m_controller.m_currentAction + "/所出的牌[";
                for (int i = 0; i < m_controller.CardsChosen.Count; i++)
                {
                    sErrorOutput += " " + i;
                }
                sErrorOutput += "] )，請重新輸入";
                Debug.LogWarning(sErrorOutput);
            }
            else
            {
                this.RefreshHandClr();
                Debug.Log("成功執行Action:" + m_controller.m_currentAction);
            }
        }
        else { this.m_hintTxt.text = "等待中"; }

    }
    private void TryToChooseAction(EHanamikojiAction _iInput)
    {
        if (this.m_controller.IsCurrentPlayer)
        {
            this.m_iCurrentCardsNeed = this.m_controller.InputAction(_iInput);
            this.m_hintTxt.text = "請挑出" + this.m_iCurrentCardsNeed + "張手牌";
            this.RefreshHandClr();
        }
        else { this.m_hintTxt.text = "等待中"; }
    }
    private void TryToChooseCard(int _iInput)
    {
        if(this.m_controller.IsCurrentPlayer)
        {
            bool bSuccess = this.m_controller.InputCardIndex(_iInput);
            if (!bSuccess) { this.m_controller.RemoveCardIndex(_iInput); }
            this.RefreshHandClr();
        }
        else { this.m_hintTxt.text = "等待中"; }
    }

    /// <summary>
    /// 更新手牌顏色(順便更新你最後到底選了哪個行動)
    /// </summary>
    private void RefreshHandClr()
    {
        for (int i = 0; i < m_listCards.Count; i++)
        {
            m_listCards[i].btn.image.color = this.m_normalCardClr;
        }
        for (int i = 0; i < m_controller.CardsChosen.Count; i++)
        {
            int iChosen = m_controller.CardsChosen[i];
            m_listCards[iChosen].btn.image.color = this.m_chosenCardClr;
        }
        for (int i = 0; i < m_listActionBtn.Count; i++)
        {
            if (i == (int)m_controller.m_currentAction) { m_listActionBtn[i].image.color = this.m_chosenActionClr; }
            else { m_listActionBtn[i].image.color = this.m_normalCardClr; }
        }
    }
}
