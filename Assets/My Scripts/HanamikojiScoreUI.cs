﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HanamikojiScoreUI : MonoBehaviour
{
    public GeishaInfoUI m_infoPrefab;

    [SerializeField]
    private List<GeishaInfoUI> m_listInfo;
    [SerializeField]
    private Vector2 m_infoSpawnBasePos = new Vector2(0, 0);
    [SerializeField]
    private Vector2 m_infoInterval = new Vector2(100, 0);
    [SerializeField]
    private HanamikojiGM m_observingGM;

    private void Start()
    {
        this.m_observingGM.GeishaUpdateCallback += this.SetGeishaInfoView;
        this.SetGeishaInfoView(m_observingGM.AllGeishas);
    }

    public void SetGeishaInfoView(List<Geisha> _listGeisha)
    {
        Vector2 startPos = m_infoSpawnBasePos - (m_infoInterval *((_listGeisha.Count-1)/2.0f) );
        while (m_listInfo.Count < _listGeisha.Count)
        {
            GeishaInfoUI infoTemp = GameObject.Instantiate(m_infoPrefab);
            infoTemp.transform.SetParent(this.transform);
            infoTemp.gameObject.SetActive(false);
            this.m_listInfo.Add(infoTemp);
        }
        for(int i = 0; i < m_listInfo.Count; i++)
        {
            if(i < _listGeisha.Count)
            {
                m_listInfo[i].transform.localPosition = startPos + (m_infoInterval * i);
                m_listInfo[i].gameObject.SetActive(true);
                m_listInfo[i].SetGeishaView(_listGeisha[i]);
            }
            else
            {
                m_listInfo[i].gameObject.SetActive(false);
            }
        }
    }
}