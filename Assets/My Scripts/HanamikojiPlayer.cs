﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 花見小路中可進行的幾個動作(密約/取捨/贈予/競爭)
/// </summary>
public enum EHanamikojiAction
{
    SECRET_MEETING = 0,
    TRADE_OFF = 1,
    GIFT = 2,
    COMPETITION = 3
}

/// <summary>
/// 玩家持有的所有動作代幣(用完後下翻，一輪對局當中每個動作都會執行一次)
/// </summary>
[Flags]
public enum EActionTokens
{
    SECRET_MEETING = 1,
    TRADE_OFF = 2,
    GIFT = 4,
    COMPETITION = 8
}

//桌遊 "花見小路" 的玩家
public class HanamikojiPlayer : CardsHolder
{
    public Action<HanamikojiPlayer> PlayerUpdateCallback;
    public EActionTokens ActionTokensLeft { get; protected set; }
    public List<HanamikojiGift> GiftsInHand
    {
        get
        {
            List<HanamikojiGift> listGiftsOutput = new List<HanamikojiGift>();
            for(int i = 0; i < base.Cards.Count; i++)
            {
                listGiftsOutput.Add((HanamikojiGift)base.Cards[i]);
            }
            return listGiftsOutput;
        }
    }
    public override void AddCard(ICard _newCard, bool _bCopy = false)
    {
        base.AddCard(_newCard, _bCopy);
        this.PlayerUpdateCallback?.Invoke(this);
    }

    /// <summary>
    /// 打出卡牌的事件，需要回報 "哪個玩家打的/執行哪個動作/打出了哪些牌" ，基本上交給GM來註冊
    /// </summary>
    public Action<HanamikojiPlayer, EHanamikojiAction, List<ICard>> PlayCardsCallback;
    /// <summary>
    /// 回應對手的事件，需要回報 "回應者/回應什麼動作/選了哪張牌"
    /// </summary>
    public Action<HanamikojiPlayer, EHanamikojiAction, int > ResponseOpponentCallback;
    /// <summary>
    /// 對手送到你面前的卡牌 (例如贈予或競爭)
    /// </summary>
    public List<HanamikojiGift> CardsFromOpponent { get; protected set; }
    /// <summary>
    /// 對手進行的動作 (假設多人遊玩的話，這邊通常是指你的上一家)
    /// </summary>
    public EHanamikojiAction OpponentAction { get; protected set; }
    /// <summary>
    /// 表示你密約了哪一張牌
    /// </summary>
    public HanamikojiGift SecretGift { get; protected set; }
    public List<HanamikojiGift> DiscardGifts { get; protected set; }

    /*ActionHandler(HanamikojiGM)需要幾個資料? 
     * (打出了哪些牌/執行什麼行動)
     * 密約:1張牌，GM將此牌放入該玩家的密牌區
     * 取捨:2張牌，GM將這些牌放入本賽局的棄牌堆(基本上不公開，但可能輕鬆模式，捨棄的那個玩家可以去看)
     * 贈予:3張牌，GM將這些牌放到緩衝區(?)，等待另一位玩家取得他要的牌後再去布置
     * 競爭:4張牌，GM將這些牌放到緩衝區，讓另一位玩家挑其中一組 (資料面估計讓玩家直接選擇說前兩張自己一組)
     */

    public HanamikojiPlayer() : base()
    {
        this.ActionTokensLeft = new EActionTokens();
        this.DiscardGifts = new List<HanamikojiGift>();
        this.ActionTokensLeft = (EActionTokens)15; //(1+2+4+8，每個選項都先設為可用)
    }
    /// <summary>
    /// 一次完整的出牌
    /// </summary>
    /// <param name="_action">執行的動作</param>
    /// <param name="_iArrIndex">挑出哪幾個手牌</param>
    public void PlayAction(EHanamikojiAction _action, List<int> _iListIndex)
    {
        int iPlayerTokensValue = (int)this.ActionTokensLeft;
        int iInputActionValue = 1 << (int)_action;
        bool bArrayNormal = true; //選牌的陣列是否正常(挑選的目標是否無重複)
        for(int i = 0; i < _iListIndex.Count && bArrayNormal; i++)
        {
            if (_iListIndex[i] < 0 || _iListIndex[i] >= base.Cards.Count) { bArrayNormal = false; }
            for (int j = i+1; j < _iListIndex.Count && bArrayNormal; j++)
            {
                if(_iListIndex[i] == _iListIndex[j]) { bArrayNormal = false; }
            }
        }

        if ( (iPlayerTokensValue & iInputActionValue) > 0 && bArrayNormal 
            && _iListIndex.Count == HanamikojiPlayer.GetActionCardsNum(_action) )
        {//在上面我們確認你還有這個動作可執行後才會去觸發事件 (另外也去判斷挑選的牌是否重複或超出範圍)
            this.ActionTokensLeft -= iInputActionValue;
            List<ICard> cardsPlayed = new List<ICard>();
            for (int i = 0; i < _iListIndex.Count; i++)
            {
                cardsPlayed.Add( base.Cards[_iListIndex[i]]);
            }
            for(int i = 0; i < cardsPlayed.Count; i++)
            {
                base.Cards.Remove(cardsPlayed[i]);
            }
            Debug.Log("Player這邊確認執行Action " + _action);
            this.PlayCardsCallback?.Invoke(this, _action, cardsPlayed);
            this.PlayerUpdateCallback?.Invoke(this);
        }
    }
    /// <summary>
    /// 確認玩家是否還有該行動
    /// </summary>
    /// <param name="_action"></param>
    /// <returns></returns>
    public bool CheckActionLeft(EHanamikojiAction _action)
    {
        int iPlayerTokensValue = (int)this.ActionTokensLeft;
        int iInputActionValue = 1 << (int)_action;
        return (iPlayerTokensValue & iInputActionValue) > 0;
    }
    /// <summary>
    /// 接收對手做出的指令
    /// </summary>
    /// <param name="_action"></param>
    /// <param name="_cards"></param>
    public void ReceiveAction(EHanamikojiAction _action , List<HanamikojiGift> _cards)
    {
        this.OpponentAction = _action;
        this.CardsFromOpponent = _cards;
        this.PlayerUpdateCallback?.Invoke(this);
    }
    /// <summary>
    /// 實際回應對手的指令 (挑出了哪一張牌)
    /// 競爭的話，送過來的會是List(長度4，0123)，由於你是挑其中一組，所以你在這邊會輸入0或2
    /// </summary>
    public void ResponseOpponentAction(EHanamikojiAction _action, int _iIndex)
    {
        if(_action == EHanamikojiAction.COMPETITION) { _iIndex -= (_iIndex % 2); } //行動為競爭時，強制讓玩家選偶數
        this.CardsFromOpponent = null;
        this.PlayerUpdateCallback?.Invoke(this);
        this.ResponseOpponentCallback?.Invoke(this, _action, _iIndex);
    }

    /// <summary>
    /// 帶入執行的動作，回傳 "可以挑幾張牌"
    /// </summary>
    /// <param name="_action"></param>
    /// <returns></returns>
    public static int GetActionCardsNum(EHanamikojiAction _action)
    {
        int iOutput = 0;
        switch (_action)
        {
            case EHanamikojiAction.SECRET_MEETING:
                iOutput = 1;
                break;
            case EHanamikojiAction.TRADE_OFF:
                iOutput = 2;
                break;
            case EHanamikojiAction.GIFT:
                iOutput = 3;
                break;
            case EHanamikojiAction.COMPETITION:
                iOutput = 4;
                break;
        }
        return iOutput;
    }
    public string GetStatus()
    {
        string sOutput = "";
        for (int i = 0; i < base.Cards.Count; i++)
        {
            sOutput += base.Cards[i].CardName;
            if (i + 1 < base.Cards.Count) { sOutput += "/"; }
        }
        sOutput += "\n剩餘動作:" + (int)this.ActionTokensLeft;
        return sOutput;
    }
    public void SetSecretGift(HanamikojiGift _gift)
    {
        this.SecretGift = _gift;
    }
    public void AddDiscardGifts(List<HanamikojiGift> _gifts)
    {
        for(int i = 0; i < _gifts.Count; i++)
        {
            if (!this.DiscardGifts.Contains(_gifts[i]) )
            {
                this.DiscardGifts.Add(_gifts[i]);
            }
        }
    }
}
