﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//擁有一場遊戲需要的資訊 (兩個玩家/現在由誰出牌/事件處理/處理中的卡牌/七名藝妓各獲得了哪一方多少禮物卡)
public class HanamikojiGM : MonoBehaviour
{
    public static readonly int[] CardsNumTable = new int[] { 2, 2, 2, 3, 3, 4, 5 };

    public Action<List<Geisha>> GeishaUpdateCallback;
    public Action<int , HanamikojiPlayer> PlayerUpdateCallback;
    public Action<HanamikojiGM> RoundEndingCallback;
    /*GM的事件處理
    * 需要接收的資料 (誰打出的/執行的動作/甩了哪些牌)
    * 需要CardPlayedHandle (處理說誰打了什麼牌)
    * 需要EventHandle (處理事件，例如你打了 "贈予" 後，送上三張牌，GM會判斷說要將這三張牌送到下一家面前)
    * 等待Event處理完 (玩家那邊只處理 "選禮物"，之後分配是交還給GM分配)
    */

    private const string m_sPath = "Hanamikoji";
    /// <summary>
    /// 正在處理的哪些牌 (主要是為了贈予跟競爭設立的)
    /// </summary>
    private List<HanamikojiGift> m_listCardsHandling;
    /// <summary>
    /// 廢牌區，把所有甩掉的牌整理起來，好在進行下一場遊戲時直接洗回
    /// </summary>
    private List<HanamikojiGift> m_listDiscardZone;
    /// <summary>
    /// 現在是哪個玩家的回合
    /// </summary>
    private int m_iCurrentPlayer = 0;
    /// <summary>
    /// 玩家數量 (基本上都是2)
    /// </summary>
    private int m_iPlayerNum = 2;
    /// <summary>
    /// 開場手牌數 (基本上都是6)
    /// </summary>
    private int m_iStartHandSize = 6;
    private List<HanamikojiPlayer> m_listPlayers;
    private List<HanamikojiController> m_listControllers;
    private Deck m_hanamikojiDeck;
    /// <summary>
    /// 全部的藝妓 (通常為7個)
    /// </summary>
    public List<Geisha> AllGeishas { get; set; }
    public List<HanamikojiPlayer> AllPlayers { get { return this.m_listPlayers; } }
    public List<HanamikojiController> AllControllers { get {return this.m_listControllers; } }

    private void Start()
    {
        this.AllGeishas = new List<Geisha>();
        this.m_listPlayers = new List<HanamikojiPlayer>();
        this.m_listControllers = new List<HanamikojiController>();
        List<float> fListPlayersFavorbility = new List<float>();
        this.m_listCardsHandling = new List<HanamikojiGift>();
        for (int i = 0; i < m_iPlayerNum; i++)
        {
            this.m_listPlayers.Add(new HanamikojiPlayer());
            this.m_listControllers.Add(new HanamikojiController(m_listPlayers[i]));
            fListPlayersFavorbility.Add(0);
        }
        int iGeishaTypesNum = Enum.GetNames(typeof(EGeishaType)).Length;
        for (int i = 0; i < iGeishaTypesNum; i++)
        {
            //藝妓初始化
            this.AllGeishas.Add(new Geisha(fListPlayersFavorbility, (EGeishaType)i));
            Debug.Log(AllGeishas[i].ToString());
        }
        this.GeishaUpdateCallback?.Invoke(this.AllGeishas);

        #region 測試藝妓好感度是否為Deep copy
        //List<float> fListPlayersFavorbility = new List<float>() { 1.1f, 2.2f };
        //Debug.Log("各玩家好感度: " + fListPlayersFavorbility[0] + " " + fListPlayersFavorbility[1]);
        //this.AllGeishas.Add(new Geisha(fListPlayersFavorbility, 2));
        //Debug.Log("藝妓現在擁有的各玩家好感度:" + this.AllGeishas[0].PlayersFavorability[0] + " " + this.AllGeishas[0].PlayersFavorability[1]);
        //fListPlayersFavorbility[0] = 0;
        //Debug.Log("GM調整後，各玩家好感度: " + fListPlayersFavorbility[0] + " " + fListPlayersFavorbility[1]);
        //Debug.Log("藝妓是否受影響:" + this.AllGeishas[0].PlayersFavorability[0] + " " + this.AllGeishas[0].PlayersFavorability[1]);
        //2020/03/22 確認了目前是Deep copy
        #endregion 測試藝妓好感度是否為Deep copy

        #region 牌堆&手牌初始化
        this.InitHanamikojiDeck();
        for (int i = 0; i < this.m_listPlayers.Count; i++)
        {
            this.PlayerDrawCard(i, m_iStartHandSize);
            Debug.Log(this.m_listPlayers[i].GetStatus());
        }
        Debug.Log("抽牌之後:\n" + this.m_hanamikojiDeck.GetAllCardName());
        #endregion 牌堆&手牌初始化

        for (int i = 0; i < m_listPlayers.Count; i++)//註冊玩家的事件聆聽
        {
            this.m_listPlayers[i].PlayCardsCallback += this.HandlePlayEvent;
            this.m_listPlayers[i].ResponseOpponentCallback += this.HandleResponseEvent;
        }

        #region 遊玩測試(先直接幫玩家帶入參數)
        this.m_iCurrentPlayer = 0;
        this.PlayerDrawCard(m_iCurrentPlayer, 1);
        this.m_listControllers[m_iCurrentPlayer].SetCurrentPlayer(true);
        Debug.Log(this.m_listPlayers[m_iCurrentPlayer].GetStatus());
        this.PlayerUpdateCallback?.Invoke(m_iCurrentPlayer , this.m_listPlayers[m_iCurrentPlayer]);

        //this.m_listPlayers[0].PlayAction(EHanamikojiAction.COMPETITION, new List<int> { 0, 2, 1,3 });
        //this.m_listPlayers[1].ResponseOpponentAction(EHanamikojiAction.COMPETITION , 2);
        //for(int i = 0; i < AllGeishas.Count; i++)
        //{
        //    Debug.Log(AllGeishas[i].ToString());
        //}
        //Debug.Log(this.m_listPlayers[0].GetStatus());
        //Debug.Log(this.m_listPlayers[1].GetStatus());
        #endregion 遊玩測試(先直接幫玩家帶入參數)
    }
    /// <summary>
    /// 讓GM的牌堆初始化
    /// </summary>
    public void InitHanamikojiDeck()
    {
        UnityEngine.Object[] arrCardsData = Resources.LoadAll(m_sPath, typeof(HanamikojiBasicCardData));
        this.m_hanamikojiDeck = new Deck();
        for (int i = 0; i < arrCardsData.Length; i++)
        {
            //接著要依照讀取到的資訊來洗入禮物卡牌
            HanamikojiBasicCardData cardData = arrCardsData[i] as HanamikojiBasicCardData;
            if (cardData != null)
            {
                ////藝妓初始化 (原本是放在這，但後來決定直接依照EGeishaType的數值順序)
                //this.AllGeishas.Add(new Geisha(fListPlayersFavorbility, cardData.TotalNum, cardData.GeishaType));
                //Debug.Log(AllGeishas[i].ToString());

                for (int j = 0; j < cardData.TotalNum; j++)
                {
                    this.m_hanamikojiDeck.AddCard(new HanamikojiGift(cardData.GeishaType));
                }
            }
        }
        Debug.Log("花見小路牌堆初始化: " + this.m_hanamikojiDeck.GetAllCardName());
        this.m_hanamikojiDeck.Shuffle();
        Debug.Log("花見小路牌堆洗牌後:\n" + this.m_hanamikojiDeck.GetAllCardName());
    }
    /// <summary>
    /// 整體的初始化 (牌堆重洗，雙方玩家抽6張牌)
    /// </summary>
    public void InitHanamikojiGame()
    {
        
    }

    /// <summary>
    /// 指定一個玩家抽幾張牌
    /// </summary>
    /// <param name="_iPlayerNum"></param>
    /// <param name="_iDrawNum"></param>
    private void PlayerDrawCard(int _iPlayerNum, int _iDrawNum)
    {
        if (_iPlayerNum >= 0 && _iPlayerNum < m_listPlayers.Count)
        {
            for (int i = 0; i < _iDrawNum && this.m_hanamikojiDeck.Cards.Count > 0; i++)
            {
                this.m_listPlayers[_iPlayerNum].AddCard(this.m_hanamikojiDeck.DrawCard());
            }
        }
    }
    /// <summary>
    /// 處理玩家出牌後傳過來的資訊
    /// </summary>
    /// <param name="_player"></param>
    /// <param name="_action"></param>
    /// <param name="_cards"></param>
    private void HandlePlayEvent(HanamikojiPlayer _player, EHanamikojiAction _action, List<ICard> _cards)
    {
        if(this.FindIndexByRefrence(m_listPlayers , _player) != this.m_iCurrentPlayer) { return; }//若不是當前玩家，則不受理PlayEvent
        
        this.m_listCardsHandling.Clear();
        for (int i = 0; i < _cards.Count; i++)
        {
            HanamikojiGift giftTemp = _cards[i] as HanamikojiGift;
            if (giftTemp != null) { this.m_listCardsHandling.Add(giftTemp); }
        }
        int iNextPlayer = (this.m_iCurrentPlayer + 1) % m_iPlayerNum;
        this.m_listControllers[m_iCurrentPlayer].SetCurrentPlayer(false); //無論怎麼樣都會把現階段玩家的執行權關掉
        this.PlayerUpdateCallback?.Invoke(m_iCurrentPlayer, this.m_listPlayers[m_iCurrentPlayer]);
        switch (_action)
        {
            case EHanamikojiAction.SECRET_MEETING:
                _player.SetSecretGift(this.m_listCardsHandling[0]);
                this.NextTurn();
                //會自己結束掉的Action在做完後就會直接跳到下一家
                break;
            case EHanamikojiAction.TRADE_OFF:
                _player.AddDiscardGifts(this.m_listCardsHandling);
                this.NextTurn();
                break;
            case EHanamikojiAction.GIFT:
                this.m_listPlayers[iNextPlayer].ReceiveAction(_action, this.m_listCardsHandling);
                this.PlayerUpdateCallback?.Invoke(iNextPlayer, this.m_listPlayers[iNextPlayer]);
                break;
            case EHanamikojiAction.COMPETITION:
                this.m_listPlayers[iNextPlayer].ReceiveAction(_action, this.m_listCardsHandling);
                this.PlayerUpdateCallback?.Invoke(iNextPlayer, this.m_listPlayers[iNextPlayer]);
                break;
        }
    }
    /// <summary>
    /// 接收玩家的回應事件 (Response Opponent)
    /// </summary>
    /// <param name="_player"></param>
    /// <param name="_action"></param>
    /// <param name="_iIndex"></param>
    private void HandleResponseEvent(HanamikojiPlayer _player, EHanamikojiAction _action, int _iIndex)
    {
        switch (_action)
        {
            case EHanamikojiAction.GIFT:
                for (int i = 0; i < this.m_listCardsHandling.Count; i++)
                {
                    if (i == _iIndex) { this.PlayerApplyGift(_player, m_listCardsHandling[i]); }
                    else { this.PlayerApplyGift(m_listPlayers[m_iCurrentPlayer], m_listCardsHandling[i]); }
                }
                break;
            case EHanamikojiAction.COMPETITION:
                for (int i = 0; i < this.m_listCardsHandling.Count; i++)
                {
                    if ((i / 2) == (_iIndex / 2)) { this.PlayerApplyGift(_player, m_listCardsHandling[i]); }
                    else { this.PlayerApplyGift(m_listPlayers[m_iCurrentPlayer], m_listCardsHandling[i]); }
                }
                break;
        }
        this.NextTurn();
    }
    private void PlayerApplyGift(HanamikojiPlayer _player, HanamikojiGift _giftCard)
    {
        int iPlayerIndex = this.FindIndexByRefrence(this.m_listPlayers, _player);
        if (iPlayerIndex >= 0) //確認有找到了再執行
        {
            bool bGeishaFound = false;
            for (int i = 0; i < AllGeishas.Count && !bGeishaFound; i++)
            {
                if (AllGeishas[i].Type == _giftCard.GeishaType)
                {
                    AllGeishas[i].PlayersFavorability[iPlayerIndex] += _giftCard.Favorability; //基本的打牌加分(預設為1)
                    bGeishaFound = true;
                    this.GeishaUpdateCallback.Invoke(this.AllGeishas);
                }
            }
        }
        else { Debug.LogWarning("未在GM的玩家列表中找到該玩家" + _player); }
    }
    /// <summary>
    /// 根據參照(指向的記憶體位置)去判斷是否為同一個物件，沒有找到的話會回傳負數
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="_list"></param>
    /// <param name="_target"></param>
    /// <returns></returns>
    public int FindIndexByRefrence<T>(List<T> _list, T _target)
    {
        int iOutput = -1;
        for (int i = 0; i < _list.Count && iOutput < 0; i++)
        {
            if (object.ReferenceEquals(_list[i], _target)) { iOutput = i; }
        }
        return iOutput;
    }
    /// <summary>
    /// 論到下一回合
    /// </summary>
    private void NextTurn()
    {
        this.PlayerUpdateCallback?.Invoke(m_iCurrentPlayer, this.m_listPlayers[m_iCurrentPlayer]);
        this.m_listControllers[m_iCurrentPlayer].SetCurrentPlayer(false);
        if (this.CheckRoundFinish() )
        {
            this.GameEndingSettlement();
        }
        else
        {
            this.m_iCurrentPlayer = (this.m_iCurrentPlayer + 1) % this.m_iPlayerNum;
            this.PlayerDrawCard(m_iCurrentPlayer, 1);
            this.m_listControllers[m_iCurrentPlayer].SetCurrentPlayer(true);
            this.PlayerUpdateCallback?.Invoke(m_iCurrentPlayer, this.m_listPlayers[m_iCurrentPlayer]);
        }        
    }

    /// <summary>
    /// 遊戲結束時的結算
    /// </summary>
    private void GameEndingSettlement()
    {
        for(int i = 0; i < m_listPlayers.Count; i++)
        {
            this.PlayerApplyGift(m_listPlayers[i], m_listPlayers[i].SecretGift);
        }
        float fMaxFavorbility = 0;
        int iPlayerLiked = -1;
        int iOriginLiked = -1;
        for (int i= 0; i < this.AllGeishas.Count; i++)
        {
            iPlayerLiked = -1;
            iOriginLiked = this.AllGeishas[i].PlayerLiked;
            if (iOriginLiked >= 0) { AllGeishas[i].PlayersFavorability[iOriginLiked] += 0.5f; } //前一回合贏的玩家會另外得0.5分
            fMaxFavorbility = this.AllGeishas[i].PlayersFavorability[0];
            for(int j = 0; j < m_iPlayerNum; j++)
            {
                if(j+1 < m_iPlayerNum)
                {
                    if(AllGeishas[i].PlayersFavorability[j+1] > fMaxFavorbility)
                    {
                        iPlayerLiked = j + 1;
                        fMaxFavorbility = AllGeishas[i].PlayersFavorability[j + 1];
                    }
                    else if(AllGeishas[i].PlayersFavorability[j + 1] < fMaxFavorbility && j == 0)
                    {
                        iPlayerLiked = 0;
                    }
                }
            }
            AllGeishas[i].PlayerLiked = iPlayerLiked;
            //在這邊遇到狀況，決定將Geisha改為Class 
            //(原因好像是因為無法直接改動Class裡頭的Struct屬性，而List算是Class)
        }
        this.RoundEndingCallback?.Invoke(this);
        this.GeishaUpdateCallback?.Invoke(this.AllGeishas);
    }
    /// <summary>
    /// 檢查此輪遊戲是否已結束 (預設是去看說大家的行動token是否已用完)
    /// </summary>
    /// <returns></returns>
    public bool CheckRoundFinish()
    {
        bool bIsFinish = true;
        for(int i = 0; i < m_listPlayers.Count && bIsFinish; i++)
        {
            if(m_listPlayers[i].ActionTokensLeft > 0) { bIsFinish = false; }
        }
        return bIsFinish;
    }

    /// <summary>
    /// 去檢查是否完全分出勝負 (一個玩家拿到11分，或一個玩家贏得4為藝妓好感，前者優先於後者)
    /// </summary>
    /// <returns></returns>
    private bool IsGameOver()
    {

        return false;
    }
}