﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeishaInfoUI : MonoBehaviour
{    
    public static string GetGeishaFavorite(EGeishaType _gType)
    {
        string[] sAllOutput = new string[] { "日式篠笛", "古書卷軸", "舞扇", "油紙傘", "三味線", "茶具", "櫻花髮簪" };
        return sAllOutput[(int)_gType];
    }
    public Text favTxt0;
    public Text favTxt1;
    public Text geishaFavoriteTxt;
    public Image heartImg;
    public void SetGeishaView(Geisha _geisha)
    {
        favTxt0.text = "" + _geisha.PlayersFavorability[0];
        favTxt1.text = "" + _geisha.PlayersFavorability[1];
        geishaFavoriteTxt.text = GetGeishaFavorite(_geisha.Type);
        if(_geisha.PlayerLiked < 0) { heartImg.gameObject.SetActive(false); }
        else
        {
            heartImg.gameObject.SetActive(true);
            heartImg.transform.localPosition = (_geisha.PlayerLiked == 0) ? new Vector2(0, -20) : new Vector2(0, 20);
        }
    }
}
